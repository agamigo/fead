package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path/filepath"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
)

// getClient uses a Context and Config to retrieve a Token
// then generate a Client. It returns the generated Client.
func getClient(ctx context.Context, config *oauth2.Config) *http.Client {
	cacheFile, err := tokenCacheFile()
	if err != nil {
		log.Fatalf("Unable to get path to cached credential file. %v", err)
	}
	tok, err := tokenFromFile(cacheFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(cacheFile, tok)
	}
	return config.Client(ctx, tok)
}

// getTokenFromWeb uses Config to request a Token.
// It returns the retrieved Token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var code string
	if _, err := fmt.Scan(&code); err != nil {
		log.Fatalf("Unable to read authorization code %v", err)
	}

	tok, err := config.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web %v", err)
	}
	return tok
}

// tokenCacheFile generates credential file path/filename.
// It returns the generated credential path/filename.
func tokenCacheFile() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	tokenCacheDir := filepath.Join(usr.HomeDir, ".credentials")
	os.MkdirAll(tokenCacheDir, 0700)
	return filepath.Join(tokenCacheDir,
		url.QueryEscape("gmail-go-quickstart.json")), err
}

// tokenFromFile retrieves a Token from a given file path.
// It returns the retrieved Token and any read error encountered.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	t := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(t)
	defer f.Close()
	return t, err
}

// saveToken uses a file path to create a file and store the
// token in it.
func saveToken(file string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", file)
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func onlyDataParts(srv *gmail.Service, payload *gmail.MessagePart) []*gmail.MessagePart {
	var attachments []*gmail.MessagePart

	switch payload.MimeType {
	case "application/octet-stream":
		attachments = append(attachments, payload)
	case "multipart/mixed":
		for _, part := range payload.Parts {
			attachments = append(attachments, onlyDataParts(srv, part)...)
		}
	}

	return attachments
}

func main() {
	var query string
	labelFilter, _ := os.LookupEnv("FEAD_LABEL_FILTER")
	queryFilter, _ := os.LookupEnv("FEAD_QUERY_FILTER")
	secretsPath, _ := os.LookupEnv("FEAD_CREDENTIALS_FILE")

	if secretsPath == "" {
		secretsPath = "./secrets.json"
	}

	if labelFilter != "" {
		query = "label:" + labelFilter + " "
	}

	if query = query + queryFilter; query == "" {
		log.Fatalf("No email label/query provided.\n" +
			"Set either \"FEAD_LABEL_FILTER\" or \"FEAD_QUERY_FILTER\"")
	}

	ctx := context.Background()

	b, err := ioutil.ReadFile(secretsPath)
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved credentials
	// at ~/.credentials/gmail-go-quickstart.json
	config, err := google.ConfigFromJSON(b, gmail.GmailReadonlyScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(ctx, config)

	srv, err := gmail.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve gmail Client %v", err)
	}

	r, err := srv.Users.Messages.List("me").Q(query).Do()
	if err != nil {
		log.Fatalf("Unable to retrieve messages: %v", err)
	}

	for _, message := range r.Messages {
		m, err := srv.Users.Messages.Get("me", message.Id).Do()
		if err != nil {
			log.Fatalf("Unable to get attachment: %v", err)
		}

		parts := onlyDataParts(srv, m.Payload)

		for _, part := range parts {
			fmt.Printf("Filename: %v\n", part.Filename)

			attachment, err := srv.Users.Messages.Attachments.Get(
				"me", m.Id, part.Body.AttachmentId).Do()
			if err != nil {
				log.Fatalf("Unable to get attachment: %v", err)
			}

			decoded, err := base64.URLEncoding.DecodeString(attachment.Data)
			if err != nil {
				log.Fatalf("Unable to decode attachment data: %v", err)
			}

			fileout, err := os.Create(part.Filename)
			if err != nil {
				log.Fatalf("Unable to create attachment file: %v", err)
			}

			_, err = fileout.Write(decoded)
			if err != nil {
				log.Fatalf("Unable to write attachment file: %v", err)
			}

			err = fileout.Close()
			if err != nil {
				log.Fatalf("Unable to close file: %v", err)
			}
		}
	}
}
