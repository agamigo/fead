# F.E.A.D - Fetch Email Attachments Data

`fead` is a simple tool to download attachments from a mail server. You provide a
filter for messages and `fead` gets all the attachments for you.

## Status

`fead` is a work in progress. The goal is to support any IMAP mail server, with
a variety of ways to filter messages.

### Limitations

- Only supports Gmail right now
- Only configurable via environment variables
- Only downloads attachments to the current working directory

## Usage

### Install

`go get -u gitlab.com/agamigo/fead`

### Configuration

- `FEAD_CREDENTIALS_FILE` - The path to your Google API secrets JSON
- `FEAD_QUERY_FILTER` - A gmail advances search query
  - Example: `FEAD_QUERY_FILTER="from:boss@company.com progress report"`
- `FEAD_LABEL_FILTER` - A label/tag to filter messages by

One of `FEAD_QUERY_FILTER` or `FEAD_LABEL_FILTER` must be set. Both can be set
and will be combined for the filter setting.

### Example

```sh
FEAD_LABEL_FILTER="JBS Hog Sale Reports" go run fead.go
Filename: SALE_REPORT_2018-01-05-20.00.22.991000_le450r.csv
Filename: SALE_REPORT_2018-01-03-20.00.25.620000_le450r.csv
Filename: SALE_REPORT_2017-12-27-20.00.19.040000_le450r.csv
Filename: SALE_REPORT_2017-11-30-20.00.26.991000_le450r.csv
# [...]
```
